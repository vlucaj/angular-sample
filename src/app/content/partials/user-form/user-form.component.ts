import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { User } from 'src/app/core/models/user';

@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.scss"]
})
export class UserFormComponent implements OnInit {
  @Output() addUserEvent: EventEmitter<User> = new EventEmitter<User>();
  model: any = {};
  constructor() {}

  ngOnInit() {}

  addUser() {
    this.addUserEvent.emit(new User(this.model.fname, this.model.lname, this.model.age));
  }
}
