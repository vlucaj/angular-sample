import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/models/user';
import { UsersService } from 'src/app/core/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: Array<User>;
  constructor(private usersService: UsersService) {
    this.users = this.usersService.getUsers();
  }

  ngOnInit() {
  }

  addUserEvent(user: User) {
    this.usersService.addUser(user);
  }
}
