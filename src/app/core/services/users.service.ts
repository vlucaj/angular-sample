import { Injectable } from "@angular/core";
import { IUsersService } from "../interfaces/IUsersService";
import { User } from "../models/user";

@Injectable()
export class UsersService implements IUsersService {
  users: Array<User> = new Array<User>();
  constructor() {
    this.users.push(new User("Viktor", "Lucaj", 28));
    this.users.push(new User("Joe", "Smith", 35));
  }

  getUsers(): User[] {
    return this.users;
  }

  addUser(user: User) {
    this.users.push(user);
  }
}
