export class User {
    constructor(firstName: string, lastName: string, age: number) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    firstName: string;
    lastName: string;
    age: number;
}