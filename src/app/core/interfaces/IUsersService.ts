import { User } from "../models/user";

export interface IUsersService {
  getUsers(): Array<User>;
  addUser(user: User);
}
